<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'savePath' => __DIR__ . '/../tmp',
            'timeout' => 3600,
            'name' => 'frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'baseUrl' => '',
            'rules' => [
                '<controller:(post|comment)>/<id:\d+>/<action:(create|update|delete)>' => '<controller>/<action>',
                '<controller:(post|comment)>/<id:\d+>' => '<controller>/read',
                '<controller:(post|comment)>s' => '<controller>/list',
            ],
        ],
        'request' => [
            'baseUrl' => '',
        ],
/*		'lekoPayPal' => [
	        'class'  => 'leko\components\paypal\PayPal',
			'config' => [
		        'returnUrl' => '/site/success',
	            'cancelUrl' => '/site/fails',
				'version'   => 124.0,    // PayPal API version (e.g. 124.0)
			],
		],*/
		'paypal'=> [
			'class'        => 'common\classes\Paypal',
			'clientId'     => 'AfKHuOVUBsCb7uql1pnoCGIO4QOLDWZw5unwaqlkPtVr7MsgXQwPTHbnDuNbYnpkZqIyDzr9OylL5-uK',
			'clientSecret' => 'EE7wpxHVzrc4v0XSeQ-MIWhINorwPIdusj6O08Xn8cw1KGcs6Mesh3SF5htfyfOqbtPcBFEEEfzJxk8P',
			'isProduction' => false,
			 // This is config file for the PayPal system
			'config'       => [
				'http.ConnectionTimeOut' => 30,
				'http.Retry'             => 1,
				'mode'                   => 'sandbox',
				'log.LogEnabled'         => YII_DEBUG ? 1 : 0,
				//'log.FileName'           => '@frontend/runtime/logs/paypal.log',
				'log.LogLevel'           => 'FINE',
			]
		],
    ],
    'params' => $params,
];
