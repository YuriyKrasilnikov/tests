<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use common\models\LoginForm;
use common\models\TransactionService;

use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;

use common\classes\Paypal;
use common\classes\PaypalIPN;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
		$transactionService = new TransactionService();
		$count_tr = $transactionService->countTransactionByUserId(\Yii::$app->user->id);

		return $this->render('index', [
			'count_tr' => $count_tr
		]);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionSuccess()
    {
		return $this->render('success');
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionStatus()
    {
		$post = Yii::$app->request->post();

		$ipn = new PayPalIPN();
		// Use the sandbox endpoint during testing.
		$ipn->useSandbox();
		$verified = $ipn->verifyIPN();
		if ($verified)
		{
			$customData = json_decode($post['custom'], true);
			$userId = $customData['user_id'];

			//obtain information about the transaction from the database
			$transactionService = new TransactionService();
			$transaction = $transactionService->getTransactionById($post['txn_id']);

			if($transaction === null)
			{
				$valid = true;
				// to validate the transaction
				if ($post['payment_gross'] != "10.00")
				{
				    $valid = false;
		        }
				/*
				 * Check for zero price
				 */
		        elseif ($post['payment_gross'] == 0)
				{
				    $valid = false;
		        }
				/*
				* Check your payment status
				*/
		        elseif ($post['payment_status'] !== 'Completed'){
				    $valid = false;
		        }
				/*
				* Check Payee
				*/
		        elseif ($post['receiver_email'] != 'info@easyweb.link'){
				    $valid = false;
		        }
				/*
				* currency verification
				*/
				elseif ($post['mc_currency'] == 'USD'){
					$valid = false;
				}

				if($valid)
				{
					// the payment was successful. keep transaction in a database. 
					$transactionService->createTransaction($post, $userId);
				}
				else{
					// payment failed validation. You need to check manually
				}
			}
		}

		// Reply with an empty 200 response to indicate to paypal the IPN was received correctly.
		header("HTTP/1.1 200 OK");
	}

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
