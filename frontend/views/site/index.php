<?php

/* @var $this yii\web\View */
use yii\helpers\Url;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Download file</h1>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-9">
			<p>File downloads will be available after receipt of payment in the amount of $ 10.</p>
           </div>
            <div class="col-lg-3">
				<p>
<?php if (!Yii::$app->user->isGuest) : ?>
	<?php if ($count_tr == 0) : ?>
					<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
						<input type="hidden" name="cmd" value="_xclick">
						<input type="hidden" name="business" value="info@easyweb.link">
						<input type="hidden" name="lc" value="EN">
						<input type="hidden" name="item_name" data-product-input="linked" value="Zip File">
						<input type="hidden" name="amount" data-product-input="price" value="1.00">
						<input type="hidden" name="currency_code" data-product-input="currency" value="USD">
						<input type="hidden" name="button_subtype" value="services">
						<input type="hidden" name="no_note" value="0">
						<input type="hidden" name="bn" value="PP-BuyNowBF:btn_paynowCC_LG.gif:NonHostedGuest">
						<input type="hidden" name="return" value="<?= Url::to(['/success'], TRUE) ?>">
						<input type="hidden" name="custom" value="<?= base64_encode(json_encode(['user_id' => \Yii::$app->user->id])) ?>">
						<button type="submit" style="border: none; padding: 0px; background-color: #ffffff">
							<input border="0" type="button" alt="PayPal" name="submit" value="Оплатить">
						</button>
					</form>
	<?php else : ?>
					<form action="http://my.test.upwork.ua/download.zip" method="post" target="_top">
						<button type="submit" style="border: none; padding: 0px; background-color: #ffffff">
							<input border="0" type="button" alt="PayPal" name="submit" value="Скачать">
						</button>
					</form>
	<?php endif; ?>
<?php else : ?>
				For buying and downloading file login to the Cabinet.
<?php endif; ?>
				</p>
            </div>
        </div>

    </div>
</div>
