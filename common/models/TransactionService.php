<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%transaction_service}}".
 *
 * @property integer $id
 * @property string $txn_id
 * @property string $payment_status
 * @property string $payment_date
 * @property string $payer_id
 * @property string $ipn_track_id
 * @property string $txn_type
 * @property integer $user_Id
 * @property string $text
 * @property string $created
 * @property string $updated
 */
class TransactionService extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transaction_service}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['created', 'updated'], 'safe'],
            [['txn_id', 'payment_status', 'payment_date', 'payer_id', 'ipn_track_id', 'txn_type'], 'string', 'max' => 45],
			[['user_Id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/transaction', 'ID'),
            'txn_id' => Yii::t('common/transaction', 'Txn ID'),
            'payment_status' => Yii::t('common/transaction', 'Payment Status'),
            'payment_date' => Yii::t('common/transaction', 'Payment Date'),
            'payer_id' => Yii::t('common/transaction', 'Payer ID'),
            'ipn_track_id' => Yii::t('common/transaction', 'Ipn Track ID'),
            'txn_type' => Yii::t('common/transaction', 'Txn Type'),
            'user_Id' => Yii::t('common/transaction', 'User ID'),
            'text' => Yii::t('common/transaction', 'Text'),
            'created' => Yii::t('common/transaction', 'Created'),
            'updated' => Yii::t('common/transaction', 'Updated'),
        ];
    }

	public function getTransactionById($id)
	{
		return TransactionService::find()
			->where(['`txn_id`' => $id,])
			->all();
	}

	public function countTransactionByUserId($id)
	{
		return TransactionService::find()
			->where(['`user_Id`' => $id,])
			->count();
	}

	public function createTransaction($post, $user_Id)
	{
		$data = new TransactionService();
		$data->txn_id = $post['txn_id'];
		$data->payment_status = $post['payment_status'];
		$data->payment_date = $post['payment_date'];
		$data->payer_id = $post['payer_id'];
		$data->ipn_track_id = $post['ipn_track_id'];
		$data->txn_type = $post['txn_type'];
		$data->user_Id = $user_Id;
		$data->text = base64_encode(json_encode($post));
		if ($data->save()) return true;

		return false;
	}
}
